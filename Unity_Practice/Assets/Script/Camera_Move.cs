﻿using UnityEngine;
using System.Collections;

public class Camera_Move : MonoBehaviour {
	public float Speed = 3.0f;

	void Start ()
	{
	//部屋が汚いんじゃない 俺が美しいんだ
	}

	void Update () 
	{
        //motimotinoki
		if (Input.GetKey(KeyCode.Alpha1)) { transform.position += new Vector3(-1.0f, 0.0f, 0.0f) * Speed * Time.deltaTime; }
		if (Input.GetKey(KeyCode.Alpha4)) { transform.position += new Vector3(1.0f, 0.0f, 0.0f) * Speed * Time.deltaTime; }
		if (Input.GetKey(KeyCode.Alpha2)) { transform.position += new Vector3(0.0f, 1.0f, 0.0f) * Speed * Time.deltaTime; }
		if (Input.GetKey(KeyCode.Alpha3)) { transform.position += new Vector3(0.0f, -1.0f, 0.0f) * Speed * Time.deltaTime; }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(new Vector3(0, 1.0f, 0.0f));
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(new Vector3(0, -1.0f, 0.0f));
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(new Vector3(-1.0f, 0.0f, 0.0f));
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(new Vector3(1.0f, 0.0f, 0.0f));
        }
	}
}
