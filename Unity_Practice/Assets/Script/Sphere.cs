﻿using UnityEngine;
using System.Collections;

public class Sphere : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        var vec = Vector3.zero;

        if (Input.GetKey(KeyCode.F)) vec.x -= 3.0f;
        if (Input.GetKey(KeyCode.H)) vec.x += 3.0f;
        if (Input.GetKey(KeyCode.T)) vec.y += 3.0f;
        if (Input.GetKey(KeyCode.G)) vec.y -= 3.0f;

        transform.position += vec * Time.deltaTime;
	}
}
