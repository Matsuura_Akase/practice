﻿using UnityEngine;
using System.Collections;

public class Cube_Move : MonoBehaviour {
	public float Speed = 3.0f;

	void Start () 
	{
	
	}

	void Update () 
	{
		if (Input.GetKey(KeyCode.A)) { transform.position += new Vector3(-1.0f, 0.0f, 0.0f) * Speed * Time.deltaTime; }
		if (Input.GetKey(KeyCode.D)) { transform.position += new Vector3(1.0f, 0.0f, 0.0f) * Speed * Time.deltaTime; }
		if (Input.GetKey(KeyCode.W)) { transform.position += new Vector3(0.0f, 1.0f, 0.0f) * Speed * Time.deltaTime; }
		if (Input.GetKey(KeyCode.S)) { transform.position += new Vector3(0.0f, -1.0f, 0.0f) * Speed * Time.deltaTime; }

	}
}
